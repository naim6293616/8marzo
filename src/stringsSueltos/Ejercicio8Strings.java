package stringsSueltos;

public class Ejercicio8Strings {
	
	public static boolean esPalindrome(String s) {
		String nueva = "";
		for (int i = 0; i < s.length(); i++) {
			nueva = s.charAt(i) + nueva;
		}
		if (nueva.equals(s)) {
			return true;
		}
		return false;
	}

	public static void main(String[] args) {
		
		System.out.println(esPalindrome("ana"));
		
	}
}
