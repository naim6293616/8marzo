package stringsSueltos;

public class Ejercicio6Definitivo {
	
	public static boolean esPrefijo(String prefijo, String s) {
		for (int i = 0; i < prefijo.length(); i++) {
			if (prefijo.charAt(i) != s.charAt(i)) {
				return false;
			}
		}
		return true;
	}
	
	public static boolean esPrefijoDesde(String prefijo, String s, int posicion) {
		String nueva = "";
		for (int i = 0; i < s.length(); i++) {
			if (i >= posicion) {
				nueva = nueva + s.charAt(i);
			}
		}
		if (esPrefijo(prefijo, nueva)) {
			return true;
		}
		return false;
	}

	public static void main(String[] args) {
		
		System.out.println(esPrefijoDesde("bi", "superbimodal", 5));
		
	}

}
