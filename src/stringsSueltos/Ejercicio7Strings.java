package stringsSueltos;

public class Ejercicio7Strings {
	
	public static boolean esPrefijo(String prefijo, String s) {
		for (int i = 0; i < prefijo.length(); i++) {
			if (prefijo.charAt(i) != s.charAt(i)) {
				return false;
			}
		}
		return true;
	}
	
	public static boolean esPrefijoDesde(String prefijo, String s, int posicion) {
		String nueva = "";
		for (int i = 0; i < s.length(); i++) {
			if (i >= posicion) {
				nueva = nueva + s.charAt(i);
			}
		}
		if (esPrefijo(prefijo, nueva)) {
			return true;
		}
		return false;
	}
	
	public static boolean subString(String s1, String s2) {
		if (s2.length() < s1.length()) {
			return false;
		}
		for (int i = 0; i < s2.length(); i++) {
			if (esPrefijoDesde(s1, s2, i)) {
				return true;
			}
		}
		return false;
	}

	public static void main(String[] args) {

		System.out.println(subString("contenido", "acontenido"));
		
	}

}
