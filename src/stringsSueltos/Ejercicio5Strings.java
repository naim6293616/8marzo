package stringsSueltos;

public class Ejercicio5Strings {
	
	public static boolean esPrefijo(String prefijo, String s) {
		for (int i = 0; i < prefijo.length(); i++) {
			if (prefijo.charAt(i) != s.charAt(i)) {
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args) {
		
		System.out.println(esPrefijo("anti", "antisocial"));

	}

}
