package stringsSueltos;

public class Ejercicio4Strings {
	
	public static boolean esDuodroma(String s) {
		if (s.length() % 2 != 0) {
			return false;
		} else {
			for (int i = 0; i < s.length(); i = i + 2) {
				if (s.charAt(i) != s.charAt(i + 1)) {
					return false;
				}
			}
		}
		return true;
	}

	public static void main(String[] args) {

		System.out.println(esDuodroma("ssaabb"));
		
	}

}
