package stringsSueltos;

public class Ejercicio1Strings {
	
	public static int cantApariciones(String s) {
		int cont = 0;
		for (int i = 0; i < s.length(); i++) {    
			if (s.charAt(i) == 'e') {
				cont++;
			}
		}
		return cont;
	}

	public static void main(String[] args) {
		
		System.out.println(cantApariciones("naim")); 
		
	}

}
