package stringsSueltos;

public class Ejercicio2Strings {
	
	public static boolean composicion(String s) {
		for (int i= 0; i < s.length(); i++) {
			if (s.charAt(i) != 'e') {
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args) {
		
		System.out.println(composicion("EEEEEE"));

	}

}
