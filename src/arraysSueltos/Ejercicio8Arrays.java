package arraysSueltos;

public class Ejercicio8Arrays {
	
	public static void imprimirArray(int[] a) {
		for (int i = 0; i< a.length; i++) {
			System.out.println(a[i]);
		}
	}
	
	public static int[] quitar(int[] a, int i) {
		int[] b = new int[a.length];
		for (int e = 0; e < a.length; e++) {
			if (e != i) {
				b[e] = a[e];
			}
		}
		return b;
	}

	public static void main(String[] args) {
		
		int[] a = new int[5];
		a[0] = 2;
		a[1] = 1;
		a[2] = 9;
		a[3] = 7;
		a[4] = 5;
		imprimirArray(quitar(a, 2));
		
	}

}
