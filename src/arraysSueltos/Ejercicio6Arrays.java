package arraysSueltos;

public class Ejercicio6Arrays {
	
	public static void imprimirArray(int[] a) {
		for (int i = 0; i< a.length; i++) {
			System.out.println(a[i]);
		}
	}
	
	public static int[] reverso(int[] a) {
		int[] b = new int[a.length];
		int c = 1;
		for (int i = 0; i < a.length; i++) {
			b[b.length - c] = a[i];
			c++;
		}
		return b;
	}

	public static void main(String[] args) {

		int[] a = new int[5];
		a[0] = 2;
		a[1] = 1;
		a[2] = 9;
		a[3] = 7;
		a[4] = 5;
		imprimirArray(reverso(a));
		
	}

}
