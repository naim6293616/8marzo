package arraysSueltos;

public class Ejercicio5Arrays {
	
	public static int max(int[] a) {
		int max = a[0];
		for (int i = 0; i < a.length; i++) {
			if (a[i] > max) {
				max = a[i];
			}
		}
		return max;
	}

	public static void main(String[] args) {
		
		int[] a = new int[5];
		a[0] = 2;
		a[1] = 1;
		a[2] = 9;
		a[3] = 7;
		a[4] = 5;
		System.out.println(max(a));
		
	}

}
