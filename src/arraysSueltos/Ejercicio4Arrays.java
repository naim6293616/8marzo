package arraysSueltos;

public class Ejercicio4Arrays {
	
	public static double promedio(int[] a) {
		int total = 0;
		for (int i = 0; i < a.length; i++) {
			total = total + a[i];
		}
		return (total / a.length);
	}

	public static void main(String[] args) {
		
		int[] a = new int[3];
		a[0] = 10;
		a[1] = 7;
		a[2] = 4;
		System.out.println(promedio(a));
		
	}

}
