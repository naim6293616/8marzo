package arraysSueltos;

import java.util.Scanner;

public class Ejercicio2Arrays {
	
	public static void imprimirArray(int[] a) {
		for (int i = 0; i< a.length; i++) {
			System.out.println(a[i]);
		}
	}
	
	public static int[] pedirArray(int n) {
		int[] a = new int[n];
		for (int i = 0; i < a.length; i++) {
			Scanner scan = new Scanner(System.in);
			System.out.println("Que valor quiere ingresar en la posicion " + i);
			a[i] = scan.nextInt();
		}
		return a;
	}
	
	public static void main(String[] args) {
		
		imprimirArray(pedirArray(5));
		System.out.println("hola");
		
	}

}
