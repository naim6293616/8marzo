package arraysSueltos;

public class Ejercicio9Arrays {
	
	public static void imprimirArray(int[] a) {
		for (int i = 0; i< a.length; i++) {
			System.out.println(a[i]);
		}
	}
	
	public static int[] opuestos(int[] a) {
		int[] b = new int[a.length];
		for (int i = 0; i < a.length; i++) {
			b[i] = a[i] * -1;
		}
		return b;
	}

	public static void main(String[] args) {
		
		int[] a = new int[5];
		a[0] = 2;
		a[1] = 1;
		a[2] = 9;
		a[3] = -7;
		a[4] = 5;
		imprimirArray(opuestos(a));
		
	}

}
