package arraysSueltos;

public class Ejercicio3Arrays {
	
	public static void imprimirArray(int[] a) {
		for (int i = 0; i< a.length; i++) {
			System.out.println(a[i]);
		}
	}
	
	public static int[] rango(int m, int n) {
		int[] a = new int[(n-m)+1];
		int c = 0;
		for (int i = 0; i < a.length; i++) {
			a[i] = m + c;
			c++;
		}
		return a;
	}

	public static void main(String[] args) {
		
		imprimirArray(rango(4, 8));
		
	}

}
