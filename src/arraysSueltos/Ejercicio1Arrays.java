package arraysSueltos;

public class Ejercicio1Arrays {
	
	public static void imprimirArray(int[] a) {
		for (int i = 0; i< a.length; i++) {
			System.out.println(a[i]);
		}
		
		
	}

	public static void main(String[] args) {
		
		int[] a = new int[4];
		a[2] = 3;
		a[0] = 4;
		a[1] = 7;
		a[3] = 10;
		imprimirArray(a);
				
	}

}
