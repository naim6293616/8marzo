package arraysSueltos;

public class Ejercicio7Arrays {
	
	public static void imprimirArray(int[] a) {
		for (int i = 0; i< a.length; i++) {
			System.out.println(a[i]);
		}
	}
	
	public static int[] agregarAtras(int[] a, int item) {
		int[] b = new int[a.length + 1];
		for (int i = 0; i < a.length; i++) {
			b[i] = a[i];
		}
		b[b.length-1] = item;
		a = b;
		return b;
	}

	public static void main(String[] args) {
		
		int[] a = new int[5];
		a[0] = 2;
		a[1] = 1;
		a[2] = 9;
		a[3] = 7;
		a[4] = 5;
		imprimirArray(agregarAtras(a, 6));
		
	}

}
